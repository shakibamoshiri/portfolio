CSS ?= bulma.css

inputs = $(wildcard src/*.md)
pages = $(subst src,public,$(inputs:.md=.html))
svgs = $(subst src,public,$(wildcard src/*.svg))
pngs = $(subst src,public,$(wildcard src/*.png))
jpgs = $(subst src,public,$(wildcard src/*.jpg))
imgs = $(svgs) $(pngs) $(jpgs)

all: $(pages) $(imgs) public/robots.txt public/sitemap.xml

$(svgs): public/%.svg: src/%.svg
	cp $< $@
$(pngs): public/%.png: src/%.png
	cp $< $@
$(jpgs): public/%.jpg: src/%.jpg
	cp $< $@
$(pages): src/std.tmpl src/bio.tmpl
$(pages): public/%.html: src/%.md
	pandoc --from=markdown+yaml_metadata_block \
		--standalone \
		--css=$(CSS) \
		--template=src/std.tmpl \
		--shift-heading-level-by=1 \
		--toc --toc-depth 2 \
		--syntax-definition=res/syntax/zig.xml \
		$< > $@

public/bulma.css:
	cp res/bulma-0.9.3/css/bulma.css public/bulma.css
	cp res/bulma-0.9.3/css/bulma.css.map public/bulma.css.map

public/robots.txt: src/robots.txt
	cp src/robots.txt public/robots.txt

page-names := $(subst public/,,$(pages))
page-names := $(subst .html,,$(page-names))
page-names := $(filter-out index,$(page-names))
public/sitemap.xml: $(pages)
	script/sitemap.sh $(page-names) > $@

build/self-serve: src/self-serve.c
	gcc -fanalyzer -fno-omit-frame-pointer -fsanitize=address,undefined -Wall -Wextra $< -o $@

clean: $(pages)
	rm $(pages) public/sitemap.xml
