---
title: Recording your screen on (SUSE) Linux
description: Options for recording your screen on Linux (X) and editing the results
---

It is 2020 and this is still not as easy as it should be. Somehow recording my
screen with myself on webcam is a pain. Possibly the easiest way is actually
to use [Jitsi](https://jitsi.org/) or similar. That is, to use your *web*
browser; Chromium or FireFox.

::: { .message .is-warning }
:::: { .message-body }

It is now 2022 and I am now using [OBS Studio](#obs) with wlrobs on
Sway/wlroots/Wayland. This is much better than the other options
listed here.

::::
:::


However I have this strange desire to do things locally without FireFox. At
least for now anyway. FireFox *is* an interesting virtual machine and development
environment xD.

I'm using openSuSE Tumbleweed, but the package names are the same on most RPM
based distros or similar on Debian.

# Outline #

+ **vokoscreenNG** Recording the screen and web cam
+ **H.264** encoding for video and **Vorbis** for audio
+ **VLC** for video playback
+ **ffmpeg** for clipping, cropping, concatenating and re-encoding the videos

Alternatively to **vokoscreenNG**:

+ **guvcview** for displaying the webcam video (and crashing X, probably)
+ **Simplescreenrecorder** for recording the screen

# Setup #

Codecs are the bane of Linux distributions everywhere and openSUSE is
no exception. H.264 is by far the best that I have tried and ofcourse
it is on the naughtly list. So the first thing you *must* do is enable
the packman or VLC repository in Yast or [with
Zypper](https://en.opensuse.org/Additional_package_repositories) and
force an update of any existing multimedia pacakges to that
repository.

Then we need to [add some
packages](https://en.opensuse.org/SDB:Installing_codecs_from_Packman_repositories)
like:

```zsh
sudo zypper in gstreamer-plugins-bad-orig-addon gstreamer-plugins-ugly-orig-addon
sudo zypper in simplescreenrecorder vlc guvcview ffmpeg
```

I'm actually not sure which package contains the H.264 *gubbins* which
Simplescreenrecorder uses because I installed so much crap in the process of
discovery.

# Recording #

I think **vokoscreenNG** (or **Simplescreenrecorder** and **guvcview**) are
fairly self explanatory. Just start them and click around or look at the
`--help` if you can't stand GUIs. Just note that
**vokoscreenNG**/**Simplescreenrecorder** may not default to H.264.

Previously on my videos I was trying to do them in one shot, like a live
presentation. Which is good practice, but time consuming (and I was
experiencing some crashes), so I now set
**Simplescreenrecorder**/**vokoscreenNG** to save the video as chunks and I
clip them and stitch them together afterwards.

For webcam I simply have ~~guvcview~~ **vokoscreenNG** display the output on
the desktop. **guvcview** works OK except that the graphics driver
occasionally *throws a wobbly* due to a buffer being filled or something and
crashes **X**. Probably I should do something about that...

I also tried recording with **ffmpeg**, but performance seemed worse, audio
and video were out of sync and so on. **vokoscreenNG** is not without problems
either, at least not on the **i3** desktop where it is unable to select a
subsection of the screen for recording. So I had to crop the video afterwards.

# Editing

I really didn't want to *lay ruin upon* my package manager by installing 5
different video editing packages to see what worked. So I just use a
combination of **VLC** to watch the video and **ffmpeg** to concatenate the
clips together.

## Failed attempt

*This failed because `inpoint` and `endpoint` had no appreciable effect*

As befits a staple, bloated, *all things to all people* tool like **ffmpeg**
there are at least three different ways to join some videos together. I'm not
doing anything fancy, so I use the simplest.

```zsh
tee vids.txt <<EOF
file vid1.mkv
inpoint 00:00:01.000
outpoint 00:00:05.000
file vid2.mkv
file vid3.mkv
outpoint 00:00:30.000
EOF
ffmpeg -f concat -i vids -c copy final.mkv
```

~~I can clip the videos with `inpoint` and `endpoint`~~. I'm not interested in
making everything perfect, but I think it is best not to waste too much of the
viewers time with stuff that can be easily cut.

## Successfull attempt

The simplest way (IMO), didn't work so I had to resort `-filter_complex`. The
name is accurate; it appears **ffmpeg** implements its own stream processing
language to connect *filters* together. After some time I was able to figure
out how to chain the `concat` *filter* with `crop`.

The below concatenates the three input video segments and crops the bottom 1%
of the screen.

```zsh
ffmpeg -ss 1 -t 1:03 -i p0.mkv \
	   -ss 2 -t 13:30 -i p1.mkv \ 
	   -ss 3 -t 07:27 -i p2.mkv \
	   -filter_complex 'concat=a=1:n=3 [v][a]; [v] crop=in_w:in_h*0.99:0:0' \
	   -map '[a]' \
	   -c:a libvorbis -c:v libx264 fzsync.mkv
```

Clipping was easier to figure out, for that you just need to specify `-ss` and
`-t` on each input (`-i`) file.

Using the filter means we must re-encode the video, which is slow, but had the
added benefit of halving the overall size. So you probably shouldn't use the
*copy codec* with *concat demuxer* anyway as it won't have the best end
result.

# OBS

If you are on Tumbledweed then you will still need the packman
repository. Also you will need to install `obs-studio-devel`,
`wayland-devel`, `gcc` and `meson`.

To record a Wayland session we need the [wlrobs obs
plugin](https://hg.sr.ht/~scoopta/wlrobs). It's relatively easy to
compile and install as it has few dependencies. However there is no
package on Tumbleweed as far as I know.

OBS Studio is what [FOSDEM
recomends](https://www.ei8fdb.org/thoughts/2021/01/03/practical-advice-for-speakers-at-fosdem-2021-open-source-design-devroom/)
speakers use to record their presentations and it works pretty well.
