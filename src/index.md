---
title: Richard Palethorpe's Index
description: Richard Palethorpe's (Software Engineer) website index
bio_at_top: yes
---

# Articles

- [Bitbanging 1D Nearest-neighbor Reversible Cellular Automata](1d-reversible-automata)
- [Blockchain and why I am selling my Crypto](block-chain)
- [Creating a static site with Pandoc and Bulma](pandoc-bulma-static-site)
- [A review of tools for rolling your own C static analysis](custom-c-static-analysis-tools)
- [Fuzzy Sync: Winning a rare data race](a-rare-data-race)
- [Generating type specific deserialisers for BSON.jl](generating-type-specific-deserialisers-for-bson)
- [libactors: Actor model and message passing with Userland RCU](rcu-actors)
- [Linux socket example](linux-socket-example)
- [On creating a booking app with SvelteKit](svelte-booking-app-one)
- [Recording your screen on Linux](linux-screen-record)
- [Reproducers make the best (Linux Kernel) tests](https://medium.com/@richiejp/reproducers-make-the-best-linux-kernel-tests-1054a3ac4f4f)
- [Supporting both Linux CGroup APIs](cgroup-compat-layer)
- [Radix sort, Hash map and building C](zc-data)
- [Three ways to help your poject fail](ways-to-help-your-project-fail)
- [My life in books](https://paperbacktravels.com/interviews/richard-palethorpe-my-life-in-books)
- [100 Pull-ups and 100 dips per day challenge](pull-ups-and-dips-challenge)
- [Some data structures and algorithms in C, built and tested with Zig and Meson](zc-data)
- [Zig Vs C - Minimal HTTP server](zig-vs-c-mini-http-server)
- [Getting NodeJS 18.x to run on the nanos unikernel](nanos-clone3-brk-and-nodejs)
- [Minimal Linux VM cross compiled with Clang and Zig](zig-cross-compile-ltp-ltx-linux)

# Videos

- [Automating code review with Sparse](https://fosdem.org/2022/schedule/event/custom_c_static_analysis_with_sparse/)
- [Fuzzy Sync: Winning a rare data race](https://youtu.be/P1lstl-NwWQ)
- [How to write eBPF byte code by hand](https://youtu.be/Z6Bhhjpj1w4)
- [OpenQA with the JDP data analysis framework: Bug tag propagation on 2M+
  test results using
  Julia](https://fosdem.org/2020/schedule/event/testing_openqa_jdp/)
- [JDP Introduction: A distributed, data analysis framework in Julia](https://youtu.be/Nzha4itchg8)

# Code

Some of the projects I have been working on. You can see more on
[GitHub](https://github.com/richiejp) and
[GitLab](https://gitlab.com/Palethorpe).

- [Fuzzy Sync](https://gitlab.com/Palethorpe/fuzzy-sync): Independent edition
  of the [LTP](#Linux-Test-Project) Fuzzy Sync library. Can be used to create
  data race reproducers. [Alt repo](https://github.com/richiejp/fuzzy-sync).
- [Linux Kernel](https://www.kernel.org/): The most popular Operating System 
  kernel. In addition to helping find kernel bugs, I have also fixed a few.
  + [b9258a2cece4](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b9258a2cece4ec1f020715fe3554bc2e360f6264) `slcan: Don't transmit uninitialized stack data in padding`
  + [0ace17d56824](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=0ace17d56824165c7f4c68785d6b58971db954dd) `can, slip: Protect tty->disc_data in write_wakeup and close with RCU`
  + [redundant](https://lists.01.org/hyperkitty/list/linux-nvdimm@lists.01.org/thread/6XB2KPULIOJECOV7SXE3KFYDHGGC4KXN/)
  `nvdimm: Avoid race between probe and reading device attributes`
  + [redundant](https://lore.kernel.org/linux-mm/20201022122858.8638-1-rpalethorpe@suse.com/) `mm: memcg/slab: Stop reparented obj_cgroups from charging root` (was refused in favor of [this](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8de15e920dc85d1705ab9c202c95d56845bc2d48))
- [Linux Test Project](https://linux-test-project.github.io/): I have worked on
  the LTP for the last few years, writing tests and adding library features. For
  example...
  + [Fuzzy Sync](a-rare-data-race)
  + [C Static Analysis](https://github.com/linux-test-project/ltp/tree/master/tools/sparse)
  + [CGroup API](https://github.com/linux-test-project/ltp/blob/master/include/tst_cgroup.h).
  + [bpf_prog05](https://github.com/linux-test-project/ltp/blob/master/testcases/kernel/syscalls/bpf/bpf_prog05.c)
  `Reproducer for CVE-2021-3444 and various 32-bit DIV/MOD by zero issues in eBPF`
  + [cfs_bandwidth01](https://github.com/linux-test-project/ltp/blob/master/testcases/kernel/sched/cfs-scheduler/cfs_bandwidth01.c)
  `Reproducer for various CGroup scheduling bugs`
  + [cve-2016-7117](https://github.com/linux-test-project/ltp/blob/master/testcases/cve/cve-2016-7117.c)
  `Reproduces a use-after-free in a race between recvmmsg() and close()`
- [GFXPrim/automata](https://github.com/gfxprim/automata): GFXPrim in an ultra
  lightweight embedded graphics library created by long suffering LTP maintainer
  Cyril Chrubis. I used it to create a [reversible cellular automata viewer](1d-reversible-automata).
- [Actors.jl](https://gitlab.com/Palethorpe/Actors.jl): The actor model for 
  [Julia](https://julialang.org/). I also started a web framework based on this
  called [Luvvy](https://gitlab.com/Palethorpe/luvvy) and a
  [viral agent-based simulation](https://gitlab.com/Palethorpe/viral-agent-actor-sim).
- [libactors](https://gitlab.com/Palethorpe/libactors): C Actor model and message
  passing library using userland read-copy-update (liburcu).
- [JDP](https://gitlab.com/Palethorpe/jdp): Written in Julia; A sprawling data analysis
  'framework' I made to automatically process kernel test results and bug data. 
  I presented this at 
  [FOSDEM](https://fosdem.org/2020/schedule/event/testing_openqa_jdp/) and 
  [here](https://www.youtube.com/watch?v=Nzha4itchg8&t=13s).
- [BSONqs.jl](https://github.com/richiejp/BSONqs.jl): A fork of `BSON.jl` which uses
  Julia's meta programming features to produce type specific deserialisers dynamically.
  This allows it to deserialise BSON encoded data into native Julia structs quickly.
- [rselisp](https://gitlab.com/Palethorpe/rselisp): An insane attempt at cloning Emacs
  in Rust. It includes an elisp interpreter and a barely working text editor.
- [OpenQA & os-autoinst](http://open.qa/): A monstrosity of an operating system test
  framework. Amongst other things, I rewrote the QEMU backend to improve snapshotting
  and performance. It is mostly written in Perl (*sigh*).

