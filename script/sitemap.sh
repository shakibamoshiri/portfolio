#!/bin/sh -e

cat <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc>https://richiejp.com/</loc>
		<changefreq>weekly</changefreq>
		<priority>1.0</priority>
	</url>
EOF

for pname in $*; do
	cat <<EOF
	<url><loc>https://richiejp.com/$pname</loc></url>
EOF
done

echo '</urlset>'
